// First, we must import the schema creator
import createSchema from 'part:@sanity/base/schema-creator';
import { fromGQL, graphql } from 'sanity-graphql-schema';

const schema = graphql`
  type Project implements Document {
    name: String!
    image: Image!
    slug: String!
    isStatic: Boolean
    isFeatured: Boolean!
  }
`;

// Then we give our schema to the builder and provide the result to Sanity
export default createSchema({
  // We name our schema
  name: 'default',
  // Then proceed to concatenate our document type
  // to the ones provided by any plugins that are installed
  types: fromGQL(schema),
});
