import React from 'react';
import { icons } from 'feather-icons';

import './Sidebar.scss';

export const Sidebar: React.FC = ({ children }) => {
  return (
    <aside className="sidebar">
      <header className="sidebar-header">
        <h1>Doug Flynn Development</h1>
        <i
          className="sidebar-dropdown-arrow icon-light"
          dangerouslySetInnerHTML={{
            __html: icons['chevron-down'].toSvg({ class: 'dropdown-icon' }),
          }}
        ></i>
      </header>
    </aside>
  );
};
