import { ApolloProvider } from '@apollo/client';
import React from 'react';
import {
  BrowserRouter,
  Redirect,
  Route,
  Switch,
  withRouter,
} from 'react-router-dom';
import { apolloClient } from '../apollo/ApolloProvider';

import './Layout.scss';
import Navbar from './Navbar';
import { Sidebar } from './Sidebar';
import SideNav from './SideNav';

export const Layout: React.FC = () => {
  return (
    <ApolloProvider client={apolloClient}>
      <BrowserRouter>
        <main className="content">
          <SideNav />
          <Sidebar />
          <section className="main-content">
            <Navbar />
            <Switch>
              <Route
                path="/"
                exact
                component={() => <Redirect to="/home-page" />}
              />
              <Route
                path="/home-page"
                component={() => <div>Main content</div>}
              />
            </Switch>
          </section>
        </main>
      </BrowserRouter>
    </ApolloProvider>
  );
};
