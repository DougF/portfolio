import React from 'react';
import { icons } from 'feather-icons';

import './Navbar.scss';
import { RouteComponentProps, withRouter } from 'react-router';

type NavbarProps = RouteComponentProps;

const Navbar: React.FC<NavbarProps> = (props) => {
  return (
    <div className="navbar">
      <div className="page-title">
        <span className="hash-symbol">
          <div
            dangerouslySetInnerHTML={{
              __html: icons.hash.toSvg({ class: 'hash-svg' }),
            }}
          ></div>
        </span>
        <p className="title-text">
          {props.location.pathname === '/'
            ? 'home-page'
            : props.location.pathname.replace('/', '')}
        </p>
      </div>
    </div>
  );
};

export default withRouter(Navbar);
