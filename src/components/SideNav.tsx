import React, { useState } from 'react';
import { gql, useQuery } from '@apollo/client';
import type { Project, RootQuery } from 'src/generated/graphql';

import './SideNav.scss';
import { RouteComponentProps, withRouter } from 'react-router';
import { Link } from 'react-router-dom';

const ALL_PROJECTS_QUERY = gql`
  query AllProjectsQuery {
    allProject {
      name
      slug
      isFeatured
      image {
        asset {
          url
        }
      }
    }
  }
`;

const SideNavLink: React.FC<{ project: Project; active: boolean }> = ({
  project,
  active,
}) => {
  return (
    <div className={`sidenav-project ${active && 'active'}`}>
      <div className="project-hover-target">
        <div className="project-pill-wrapper">
          <div className="project-hover-indicator" />
        </div>
        <Link to={`/${project.slug}`}>
          <img
            src={project.image?.asset?.url || ''}
            alt={project.name || 'Project'}
          />
        </Link>
      </div>
    </div>
  );
};

type SideNavProps = RouteComponentProps;

const matchSlugToRoute = (
  slug: string | undefined | null,
  route: string,
): boolean => {
  return slug
    ? slug === route.replace('/', '') || (slug === 'profile' && route === '/')
    : false;
};

const SideNav: React.FC<SideNavProps> = ({ location }) => {
  const { data, loading } = useQuery<RootQuery>(ALL_PROJECTS_QUERY);

  return (
    <aside className="sidenav">
      {loading ? (
        <span>Loading...</span>
      ) : (
        <div className="sidenav-project-list">
          {data ? (
            <>
              {/* Render "featured" items here */}
              {
                <>
                  {data.allProject
                    .filter((p) => p.isFeatured)
                    .map((p, i) => (
                      <SideNavLink
                        active={matchSlugToRoute(p.slug, location.pathname)}
                        key={p.slug}
                        project={p}
                      />
                    ))}
                  <div className="sidenav-project">
                    <div className="sidenav-project-separator"></div>
                  </div>
                </>
              }
              {/* Render actual projects here */}
              {data.allProject
                .filter((p) => !p.isFeatured)
                .map((p) => {
                  return (
                    <SideNavLink
                      active={matchSlugToRoute(p.slug, location.pathname)}
                      key={p.slug}
                      project={p}
                    />
                  );
                })}
            </>
          ) : (
            <div>Error getting sidenav data</div>
          )}
        </div>
      )}
    </aside>
  );
};

export default withRouter(SideNav);
