import React, { useState, useEffect } from 'react';

import './App.scss';
import { Layout } from './components/Layout';

interface AppProps {}

function App({}: AppProps) {
  return (
    <div className="App">
      <Layout />
    </div>
  );
}

export default App;
