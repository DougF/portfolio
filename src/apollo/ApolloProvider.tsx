import { ApolloClient, InMemoryCache } from '@apollo/client';
import React from 'react';

export const apolloClient = new ApolloClient({
  uri: import.meta.env.SNOWPACK_PUBLIC_GRAPHQL_ENDPOINT,
  cache: new InMemoryCache(),
});
